import unittest
from calculadora_lib import suma, resta, multiplicacion, division, raizCuadrada
class testCalculadora (unittest.TestCase):

	def test_suma(self):
		# arrange
		a = 3
		b = 2
		c = 5
		#act
		resultado = suma(a, b)
		#assert
		self.assertEqual(resultado, c)

	def test_resta(self):
		#arrange
		d = 7
		e = 5
		f = 2
		#act
		resultado = resta (d, e)
		#assert
		self.assertEqual (resultado, f)


	def test_multiplicacion(self):
		#arrange
		g = 8
		h = 4
		i = 32
		#act
		resultado = multiplicacion (g, h)
		#assert
		self.assertEqual (resultado, i)


	def test_division(self):
		#arrange
		j = 8
		k = 4
		l = 2
		#act
		resultado = division (j, k)
		#assert
		self.assertEqual (resultado, l)

	def test_raizCuadrada(self):
		#arrange
		m = 254
		resultadoEsperado = 15.937377
		# act
		resultado = raizCuadrada(m)
		#assert
		self.assertLess(abs(resultado-resultadoEsperado), 0.001) 

if __name__ == '__main__':
	unittest.main()