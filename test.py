from Calculadora import suma 

def test_suma1():
	#arrange
	valor1 = 3
	valor2 = 5
	resultadoEsperado = 8
	# act
	r = suma(valor1, valor2)
	#assert
	if r == resultadoEsperado:
		print ("funcion suma correcta")
	else:
		print("Error en la función suma")

def test_suma2():
	#arrange
	valor1 = 0
	valor2 = -1
	resultadoEsperado = -1
	# act
	r = suma(valor1, valor2)
	#assert
	if r == resultadoEsperado:
		print ("funcion suma correcta")
	else:
		print("Error en la función suma")

def test_suma3():
	#arrange
	valor1 = 3
	valor2 = 2
	resultadoEsperado = 5
	# act
	r = suma(valor1, valor2)
	#assert
	if r == resultadoEsperado:
		print ("funcion suma correcta")
	else:
		print("Error en la función suma")

if __name__ == '__main__':
	test_suma1() 
	test_suma2() 
	test_suma3() 

